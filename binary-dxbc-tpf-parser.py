from sys import argv
import struct

OPCODE_NAMES = {
      0 : "ADD"                              ,
      1 : "AND"                              ,
      2 : "BREAK"                            ,
      3 : "BREAKC"                           ,
      6 : "CASE"                             ,
      7 : "CONTINUE"                         ,
      8 : "CONTINUEC"                        ,
      9 : "CUT"                              ,
     10 : "DEFAULT"                          ,
     11 : "DERIV_RTX"                        ,
     12 : "DERIV_RTY"                        ,
     13 : "DISCARD"                          ,
     14 : "DIV"                              ,
     15 : "DP2"                              ,
     16 : "DP3"                              ,
     17 : "DP4"                              ,
     18 : "ELSE"                             ,
     19 : "EMIT"                             ,
     21 : "ENDIF"                            ,
     22 : "ENDLOOP"                          ,
     23 : "ENDSWITCH"                        ,
     24 : "EQ"                               ,
     25 : "EXP"                              ,
     26 : "FRC"                              ,
     27 : "FTOI"                             ,
     28 : "FTOU"                             ,
     29 : "GE"                               ,
     30 : "IADD"                             ,
     31 : "IF"                               ,
     32 : "IEQ"                              ,
     33 : "IGE"                              ,
     34 : "ILT"                              ,
     35 : "IMAD"                             ,
     36 : "IMAX"                             ,
     37 : "IMIN"                             ,
     38 : "IMUL"                             ,
     39 : "INE"                              ,
     40 : "INEG"                             ,
     41 : "ISHL"                             ,
     42 : "ISHR"                             ,
     43 : "ITOF"                             ,
     44 : "LABEL"                            ,
     45 : "LD"                               ,
     46 : "LD2DMS"                           ,
     47 : "LOG"                              ,
     48 : "LOOP"                             ,
     49 : "LT"                               ,
     50 : "MAD"                              ,
     51 : "MIN"                              ,
     52 : "MAX"                              ,
     53 : "SHADER_DATA"                      ,
     54 : "MOV"                              ,
     55 : "MOVC"                             ,
     56 : "MUL"                              ,
     57 : "NE"                               ,
     58 : "NOP"                              ,
     59 : "NOT"                              ,
     60 : "OR"                               ,
     61 : "RESINFO"                          ,
     62 : "RET"                              ,
     63 : "RETC"                             ,
     64 : "ROUND_NE"                         ,
     65 : "ROUND_NI"                         ,
     66 : "ROUND_PI"                         ,
     67 : "ROUND_Z"                          ,
     68 : "RSQ"                              ,
     69 : "SAMPLE"                           ,
     70 : "SAMPLE_C"                         ,
     71 : "SAMPLE_C_LZ"                      ,
     72 : "SAMPLE_LOD"                       ,
     73 : "SAMPLE_GRAD"                      ,
     74 : "SAMPLE_B"                         ,
     75 : "SQRT"                             ,
     76 : "SWITCH"                           ,
     77 : "SINCOS"                           ,
     78 : "UDIV"                             ,
     79 : "ULT"                              ,
     80 : "UGE"                              ,
     81 : "UMUL"                             ,
     83 : "UMAX"                             ,
     84 : "UMIN"                             ,
     85 : "USHR"                             ,
     86 : "UTOF"                             ,
     87 : "XOR"                              ,
     88 : "DCL_RESOURCE"                     ,
     89 : "DCL_CONSTANT_BUFFER"              ,
     90 : "DCL_SAMPLER"                      ,
     91 : "DCL_INDEX_RANGE"                  ,
     92 : "DCL_OUTPUT_TOPOLOGY"              ,
     93 : "DCL_INPUT_PRIMITIVE"              ,
     94 : "DCL_VERTICES_OUT"                 ,
     95 : "DCL_INPUT"                        ,
     96 : "DCL_INPUT_SGV"                    ,
     97 : "DCL_INPUT_SIV"                    ,
     98 : "DCL_INPUT_PS"                     ,
     99 : "DCL_INPUT_PS_SGV"                 ,
    100 : "DCL_INPUT_PS_SIV"                 ,
    101 : "DCL_OUTPUT"                       ,
    103 : "DCL_OUTPUT_SIV"                   ,
    104 : "DCL_TEMPS"                        ,
    105 : "DCL_INDEXABLE_TEMP"               ,
    106 : "DCL_GLOBAL_FLAGS"                 ,
    108 : "LOD"                              ,
    109 : "GATHER4"                          ,
    110 : "SAMPLE_POS"                       ,
    111 : "SAMPLE_INFO"                      ,
    113 : "HS_DECLS"                         ,
    114 : "HS_CONTROL_POINT_PHASE"           ,
    115 : "HS_FORK_PHASE"                    ,
    116 : "HS_JOIN_PHASE"                    ,
    117 : "EMIT_STREAM"                      ,
    118 : "CUT_STREAM"                       ,
    120 : "FCALL"                            ,
    121 : "BUFINFO"                          ,
    122 : "DERIV_RTX_COARSE"                 ,
    123 : "DERIV_RTX_FINE"                   ,
    124 : "DERIV_RTY_COARSE"                 ,
    125 : "DERIV_RTY_FINE"                   ,
    126 : "GATHER4_C"                        ,
    127 : "GATHER4_PO"                       ,
    128 : "GATHER4_PO_C"                     ,
    129 : "RCP"                              ,
    130 : "F32TOF16"                         ,
    131 : "F16TOF32"                         ,
    134 : "COUNTBITS"                        ,
    135 : "FIRSTBIT_HI"                      ,
    136 : "FIRSTBIT_LO"                      ,
    137 : "FIRSTBIT_SHI"                     ,
    138 : "UBFE"                             ,
    139 : "IBFE"                             ,
    140 : "BFI"                              ,
    141 : "BFREV"                            ,
    142 : "SWAPC"                            ,
    143 : "DCL_STREAM"                       ,
    144 : "DCL_FUNCTION_BODY"                ,
    145 : "DCL_FUNCTION_TABLE"               ,
    146 : "DCL_INTERFACE"                    ,
    147 : "DCL_INPUT_CONTROL_POINT_COUNT"    ,
    148 : "DCL_OUTPUT_CONTROL_POINT_COUNT"   ,
    149 : "DCL_TESSELLATOR_DOMAIN"           ,
    150 : "DCL_TESSELLATOR_PARTITIONING"     ,
    151 : "DCL_TESSELLATOR_OUTPUT_PRIMITIVE" ,
    152 : "DCL_HS_MAX_TESSFACTOR"            ,
    153 : "DCL_HS_FORK_PHASE_INSTANCE_COUNT" ,
    154 : "DCL_HS_JOIN_PHASE_INSTANCE_COUNT" ,
    155 : "DCL_THREAD_GROUP"                 ,
    156 : "DCL_UAV_TYPED"                    ,
    157 : "DCL_UAV_RAW"                      ,
    158 : "DCL_UAV_STRUCTURED"               ,
    159 : "DCL_TGSM_RAW"                     ,
    160 : "DCL_TGSM_STRUCTURED"              ,
    161 : "DCL_RESOURCE_RAW"                 ,
    162 : "DCL_RESOURCE_STRUCTURED"          ,
    163 : "LD_UAV_TYPED"                     ,
    164 : "STORE_UAV_TYPED"                  ,
    165 : "LD_RAW"                           ,
    166 : "STORE_RAW"                        ,
    167 : "LD_STRUCTURED"                    ,
    168 : "STORE_STRUCTURED"                 ,
    169 : "ATOMIC_AND"                       ,
    170 : "ATOMIC_OR"                        ,
    171 : "ATOMIC_XOR"                       ,
    172 : "ATOMIC_CMP_STORE"                 ,
    173 : "ATOMIC_IADD"                      ,
    174 : "ATOMIC_IMAX"                      ,
    175 : "ATOMIC_IMIN"                      ,
    176 : "ATOMIC_UMAX"                      ,
    177 : "ATOMIC_UMIN"                      ,
    178 : "IMM_ATOMIC_ALLOC"                 ,
    179 : "IMM_ATOMIC_CONSUME"               ,
    180 : "IMM_ATOMIC_IADD"                  ,
    181 : "IMM_ATOMIC_AND"                   ,
    182 : "IMM_ATOMIC_OR"                    ,
    183 : "IMM_ATOMIC_XOR"                   ,
    184 : "IMM_ATOMIC_EXCH"                  ,
    185 : "IMM_ATOMIC_CMP_EXCH"              ,
    186 : "IMM_ATOMIC_IMAX"                  ,
    187 : "IMM_ATOMIC_IMIN"                  ,
    188 : "IMM_ATOMIC_UMAX"                  ,
    189 : "IMM_ATOMIC_UMIN"                  ,
    190 : "SYNC"                             ,
    191 : "DADD"                             ,
    192 : "DMAX"                             ,
    193 : "DMIN"                             ,
    194 : "DMUL"                             ,
    195 : "DEQ"                              ,
    196 : "DGE"                              ,
    197 : "DLT"                              ,
    198 : "DNE"                              ,
    199 : "DMOV"                             ,
    200 : "DMOVC"                            ,
    201 : "DTOF"                             ,
    202 : "FTOD"                             ,
    204 : "EVAL_SAMPLE_INDEX"                ,
    205 : "EVAL_CENTROID"                    ,
    206 : "DCL_GS_INSTANCES"                 ,
    210 : "DDIV"                             ,
    211 : "DFMA"                             ,
    212 : "DRCP"                             ,
    214 : "DTOI"                             ,
    215 : "DTOU"                             ,
    216 : "ITOD"                             ,
    217 : "UTOD"                             ,
}

if len(argv) not in (2,3):
    print("usage: {argv[0]} [<op-code>{,<op-code>}] <input-file>")
    exit(1)

if len(argv) > 2:
    opcodes = [int(x) for x in argv[1].split(',')]
else:
    opcodes = []
fname = argv[-1]

def as_word(bs):
    # Build the 32 bit word (little endian)
    assert(len(bs)==4)
    return bs[0] | (bs[1] << 8) | (bs[2] << 16) | (bs[3] << 24)

def as_number(bs):
    res = 0
    for b in bs[::-1]:
        res = res << 8
        res = res | b
    return res

def get_on_bin_range(word,ini,end):
    word = word << (32-end)
    word = word & 0xFFFFFFFF
    word = word >> (ini+32-end)
    return (word, bin( (1<<(end-ini) | word) )[3:])

def print_word_split(word,cuts,start="",names={}):
    cuts = set(cuts)
    cuts.add(0)
    cuts.add(32)
    cuts = sorted(list(cuts))
    text = start
    for ini,end in list(zip(cuts[:-1],cuts[1:]))[::-1]:
        val,bits = get_on_bin_range(word,ini,end)
        name = names[ini] if ini in names else None
        if name:
            if "?" in name:
                text += f"\033[0;37m{bits}[{val}]\033[0m  "
            elif "offset" in name:
                text += f"{bits}\033[0;33m[{name}:\033[1;33m{val}\033[0;33m]\033[0m  "
            else:
                text += f"{bits}\033[0;32m[{name}:{val}]\033[0m  "
        elif end-ini==32:
            flt = struct.unpack("f", struct.pack("I",word) )[0]
            text += f"{bits}\033[0;36m[{val}|{flt}]\033[0m  "
        elif end-ini>1:
            text += f"{bits}\033[0;32m[{val}]\033[0m  "
        else:
            text += f"{bits}  "
    print(text)

def format_bit_padding(bits,start=""):
    text = start
    for b in bits:
        _,bits = get_on_bin_range(int(b),0,8)
        text += f"{bits} "
    return text


def print_string(chars,start="",name=None):
    text = start

    for ch in chars:
        _,bits = get_on_bin_range(int(ch),0,8)
        text += f"{bits} "

    string = ""
    for ch in chars:
        if ch==0: break
        string += chr(ch)

    text += f"\033[0;31m\"{string}\"\033[0m"
    if name:
        text += f"\033[0;32m[{name}]\033[0m"
    print(text)


def print_word_as_register(word, prefix):
    dim = get_on_bin_range(word, 0, 2)[0]

    if dim == 0 or dim == 1:
        print_word_split(word,[0,2,12,20,22,25,28,31,32],prefix,names={0:"dim",12:"type",20:"idxs",22:"d1i",25:"d2i",28:"d3i"})
    else:
        swtype = get_on_bin_range(word, 2, 4)[0]
        if swtype == 0:
            print_word_split(word,[0,2,4,8,12,20,22,25,28,31,32],prefix,names={0:"dim",2:"swtype",4:"wmask",12:"type",20:"idxs",22:"d1i",25:"d2i",28:"d3i"})
        elif swtype == 1:
            print_word_split(word,[0,2,4,12,20,22,25,28,31,32],prefix,names={0:"dim",2:"swtype",4:"sw",12:"type",20:"idxs",22:"d1i",25:"d2i",28:"d3i"})
        else:
            print_word_split(word,[0,2,4,6,12,20,22,25,28,31,32],prefix,names={0:"dim",2:"swtype",4:"swcomp",12:"type",20:"idxs",22:"d1i",25:"d2i",28:"d3i"})

def read_next_words(file):
    words = []
    while 1:
        # Read next 4 byte word
        bs = file.read(4)
        if not bs:
            return False
        word = as_word(bs)
        # Append word
        words.append(word)
        # If it is not extended, stop reading.
        if word & (1<<31) == 0:
            break
    return words

def read_next_string(file):
    chars = []
    while 1:
        # Read next byte
        bs = file.read(1)
        if not bs:
            return False
        chars.append(bs[0])
        if bs[0] == 0:
            break
    return chars

def read_SHEX(file):
    # Read chunck lenght
    bs = file.read(4)
    if not bs: return
    length = as_word(bs)
    print_word_split(length,[0],names={0:'len'})
    n_read = 0

    # Read chunck version
    bs = file.read(4)
    if not bs: return
    n_read += 4
    print_word_split(as_word(bs),[0,4,8,16],names={0:'minorv',4:'majorv',16:'program_type'})

    # Read number of dwords in the chunck
    bs = file.read(4)
    if not bs: return
    n_read += 4
    print_word_split(as_word(bs),[0],names={0:'dwords'})

    # Read the block contents
    while n_read<length:

        words = read_next_words(file)
        if not words: break
        n_read += 4*len(words)
        # Get the opcode
        inst_opcode, _ = get_on_bin_range(words[0],0,11)
        # Check if instruction is present
        if len(opcodes)==0 or (inst_opcode in opcodes):
            print()

            if inst_opcode in OPCODE_NAMES:
                print(f"\033[0;33m> {OPCODE_NAMES[inst_opcode]} ({inst_opcode})\033[0m")

            # Print word
            print_word_split(words[0],[0,11,24,31,32],names={0:'opcode',24:'len'})
            # Print instruciton modifiers
            for w in words[1:]:
                # Instruction modifier type:
                mod_type, _ = get_on_bin_range(w,0,6)
                if mod_type==1:
                    # aoffimmi
                    print_word_split(w,[0,6,9,13,17,21,31,32],names={0:'type'})
                else:
                    print_word_split(w,[0,6,31,32],names={0:'type'})

            # Instruction length (remaining)
            inst_len, _ = get_on_bin_range(words[0],24,31)

            if inst_len == 0:
                # Special instruction where the lenght is the next word
                # (I am not sure if I have to generalize this way)
                # Only example so far is dcl_immediateConstantBuffer

                inst_len -= len(words)

                # Read next 4 byte word
                bs = file.read(4)
                if not bs:
                    return False
                n_read += 4;

                word = as_word(bs)
                print_word_split(word,[0,32],"└─ ", names = {0: "len"})

                inst_len += word
                inst_len -= 1

                for i in range(inst_len):
                    # Read next 4 byte word
                    bs = file.read(4)
                    if not bs: break
                    n_read += 4;
                    word = as_word(bs)

                    print_word_split(word,[0,32],"   └─ ")

            else:
                inst_len -= len(words)

                # Size of the register
                reg_words_len = 0
                # Number of words read from the register
                reg_words_read = 0

                for i in range(inst_len):
                    # Read next 4 byte word
                    bs = file.read(4)
                    if not bs: break
                    n_read += 4;
                    word = as_word(bs)

                    if reg_words_read==reg_words_len:
                        # Register's extended operand modifier
                        reg_mod, _ = get_on_bin_range(word,31,32)
                        # Register's Index count
                        idx_count, _ = get_on_bin_range(word,20,22)
                        # Register type
                        reg_type, _ = get_on_bin_range(word,12,20)
                        # Register dimension
                        reg_dim, _ = get_on_bin_range(word,0,2)
                        # Indexes per dimension
                        dim_idxs = [0, 0, 0]
                        dim_idxs[0], _ = get_on_bin_range(word,22,25)
                        dim_idxs[1], _ = get_on_bin_range(word,25,28)
                        dim_idxs[2], _ = get_on_bin_range(word,28,31)

                        # Words in the register that must be formatted as a register too
                        reg_words_that_are_regs = []

                        reg_words_len = 1 + reg_mod + (reg_type==4 or reg_type==5)*(1+3*(reg_dim==2))

                        for q in range(idx_count):
                            idx_len = 1 if dim_idxs[q] == 0 else dim_idxs[q]
                            if idx_len >= 2:
                                reg_words_that_are_regs.append(reg_words_len + idx_len - 2)
                            reg_words_len += idx_len
                        # Format it as a register
                        print_word_as_register(word, "└─ ")

                        reg_words_read = 0

                    elif reg_words_read in reg_words_that_are_regs:
                        # Format it as a register
                        print_word_as_register(word, "   └─ ")

                    else:
                        # Don't format it (index?)
                        print_word_split(word,[0,32],"   └─ ")

                    reg_words_read += 1


def read_RDEF(file):
    # Offsets of pending stuff to read
    offsets = {}

    start_read = file.tell()

    while 1:
        file.seek(start_read,0)
        old_len = len(offsets)
        read_RDEF_iter(file,offsets,False)
        if old_len == len(offsets):
            break

    file.seek(start_read,0)
    read_RDEF_iter(file,offsets,True)

    pass

def read_RDEF_iter(file,offsets,printing=False):
    # Read chunck length
    bs = file.read(4)
    if not bs: return
    length = as_word(bs)
    if printing: print_word_split(length,[0],names={0:'len'})
    n_read = 0

    # Read constant buffer count
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'n_cbuffers'})
    n_read += 4
    n_cbuffers = as_word(bs)

    # Read byte offset (from start of chunk data, after chunk type and chunk length) to first constant buffer description
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'offset_1st_cbuffer'})
    n_read += 4
    for i in range(n_cbuffers):
        offsets[as_word(bs) + i * 6 * 4] = "cbuffer"

    # Read resource binding count
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'n_resbinds'})
    n_read += 4
    n_resbinds = as_word(bs)

    # Read byte offset (from start of chunk data) to first resource binding description
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'offset_1st_resbind'})
    n_read += 4
    for i in range(n_resbinds):
        offsets[as_word(bs) + i * 8 * 4] = "resbind"

    # Read chunck version
    bs = file.read(4)
    if not bs: return
    n_read += 4
    if printing: print_word_split(as_word(bs),[0,8,16],names={0:'minorv',8:'majorv',16:'program_type'})
    major_v,_ = get_on_bin_range(as_word(bs),8,16)

    # Read flags
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'flags'})
    n_read += 4

    # Read byte offset (from start of chunk data) to “creator” string
    bs = file.read(4)
    if not bs: return
    if printing: print_word_split(as_word(bs),[0],names={0:'offset_creator_str'})
    n_read += 4
    offsets[as_word(bs)] = "creator"

    if major_v >= 5:
        for i in range(8):
            bs = file.read(4)
            if not bs: return
            if printing: print_word_split(as_word(bs),[0])
            n_read += 4

    # Read the block contents
    while n_read<length:
        if n_read in offsets:
            if offsets[n_read] == "cbuffer":
                if printing: print("\033[0;33m%5d > Constant buffer\033[0m"%n_read)

                # Read constant buffer name offset
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_name'})
                n_read += 4
                offsets[as_word(bs)] = "cbuffer_name"

                # Read variable count
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'n_vars'})
                n_read += 4
                n_vars = as_word(bs)

                # Read offset of 1st variable description
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_1st_vardesc'})
                n_read += 4
                for i in range(n_vars):
                    offsets[as_word(bs) + i * (10 if major_v>=5 else 6) * 4] = "var"

                # Read size of the cbuffer
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'size'})
                n_read += 4
                # Read cbuffer flags
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'flags'})
                n_read += 4
                # Read cbuffer type
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'type'})
                n_read += 4


            elif offsets[n_read] == "resbind":
                if printing: print("\033[0;33m%5d > Resource binding\033[0m"%n_read)

                # Read resource binding name offset
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_name'})
                n_read += 4
                offsets[as_word(bs)] = "resbind_name"

                # Read shader input type
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'input_type'})
                n_read += 4

                # Read resource return type
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'return_type'})
                n_read += 4

                # Read resource view dimension
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'view_dim'})
                n_read += 4

                # Read number of samples
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'n_samples'})
                n_read += 4

                # Read bind point
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'bind_point'})
                n_read += 4

                # Read bind count
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'bind_count'})
                n_read += 4

                # Read shader input flags
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'input_flags'})
                n_read += 4

            elif offsets[n_read] == "var":
                if printing: print("\033[0;33m%5d > Variable \033[0m"%n_read)

                # Read variable name offset
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_name'})
                n_read += 4
                offsets[as_word(bs)] = "var_name"

                # Read offset from start of constant buffer
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_from_start_of_cbuffer'})
                n_read += 4

                # Read variable size
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'size'})
                n_read += 4
                var_size = as_word(bs)

                # Read variable flags
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'flags'})
                n_read += 4

                # Read byte offset to variable type
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'var_type_offset'})
                n_read += 4
                offsets[as_word(bs)] = "var_type"

                # Read byte offset to default value
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'default_val_offset'})
                n_read += 4
                var_def_val_offset = as_word(bs)

                if var_def_val_offset > 0:
                    offsets[var_def_val_offset] = ("var_default_val",var_size)

                if major_v >= 5:
                    # Read register ranges
                    for i in range(4):
                        bs = file.read(4)
                        if not bs: return
                        if printing: print_word_split(as_word(bs),[0],"        └─ ",
                                names={0:['tex_start', 'tex_count', 'sam_start', 'sam_count'][i]})
                        n_read += 4

            elif offsets[n_read] == "var_type":
                if printing: print("\033[0;33m%5d > Variable type \033[0m"%n_read)

                # Read variable class and type
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,16],"        └─ ",names={0:'class',16:'type'})
                n_read += 4

                # Read columns and rows
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,16],"        └─ ",names={0:'rows',16:'cols'})
                n_read += 4

                # Read number of elements and members
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,16],"        └─ ",names={0:'n_elems',16:'n_members'})
                n_read += 4
                n_members,_ = get_on_bin_range(as_word(bs),16,32)

                # Byte offset to first member
                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,32],"        └─ ",names={0:'offset_to_1st_member'})
                n_read += 4

                for i in range(n_members):
                    offsets[as_word(bs) + i * 3 * 4] = "varfield"

                if major_v >= 5:
                    for i in range(4):
                        bs = file.read(4)
                        if not bs: return
                        if printing: print_word_split(as_word(bs),[0],"        └─ ")
                        n_read += 4

                    # Read resource binding name offset
                    bs = file.read(4)
                    if not bs: return
                    if printing: print_word_split(as_word(bs),[0],"        └─ ",names={0:'offset_name'})
                    n_read += 4
                    offsets[as_word(bs)] = "vartype_name"

            elif offsets[n_read] == "varfield":
                if printing: print("\033[0;33m%5d > Type field \033[0m"%n_read)

                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,32],"        └─ ",names={0:'offset_name'})
                n_read += 4
                offsets[as_word(bs)] = "field_name"

                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,32],"        └─ ",names={0:'offset_type'})
                n_read += 4
                offsets[as_word(bs)] = "var_type"

                bs = file.read(4)
                if not bs: return
                if printing: print_word_split(as_word(bs),[0,32],"        └─ ",names={0:'offset_reg'})
                n_read += 4

            elif offsets[n_read] == "creator":
                if printing: print("\033[0;33m%5d > Creator String \033[0m"%n_read)

                chars = read_next_string(file)
                if not chars: return
                n_read += len(chars)
                if printing: print_string(chars,"        └─ ")

            elif offsets[n_read] in ("resbind_name","cbuffer_name","var_name","vartype_name","field_name"):
                if printing: print("\033[0;33m%5d > Name \033[0m"%n_read)
                chars = read_next_string(file)
                if not chars: return
                n_read += len(chars)
                if printing: print_string(chars,"        └─ ")

            elif offsets[n_read][0] == "var_default_val":
                size = offsets[n_read][1]
                if printing: print("\033[0;33m%5d > Default variable value (size:%d) \033[0m"%(n_read,size))

                var_n_read = 0
                while var_n_read < size:
                    # Read next 4 byte word
                    bs = file.read(4)
                    if not bs: break
                    word = as_word(bs)
                    if printing: print_word_split(word,[0,32], start="        └─ ")
                    n_read += 4;
                    var_n_read += 4;
        elif (n_read+1 in offsets) or (n_read%4 == 3):
            bs = file.read(1)
            if not bs: break
            if printing: print("\033[0;33m%5d\033[0m %s"%(n_read,format_bit_padding(bs)))
            n_read += 1

        elif (n_read+2 in offsets) or (n_read%4 == 2):
            bs = file.read(2)
            if not bs: break
            if printing: print("\033[0;33m%5d\033[0m %s"%(n_read,format_bit_padding(bs)))
            n_read += 2

        elif (n_read+3 in offsets) or (n_read%4 == 1):
            bs = file.read(3)
            if not bs: break
            if printing: print("\033[0;33m%5d\033[0m %s"%(n_read,format_bit_padding(bs)))
            n_read += 3

        else:
            # Read next 4 byte word
            bs = file.read(4)
            if not bs: break
            word = as_word(bs)
            if printing: print_word_split(word,[0,32], start="\033[0;33m%5d\033[0m "%n_read)
            n_read += 4;

def read_generic_chunk(file, printing):
    # Read chunck lenght
    bs = file.read(4)
    if not bs: return
    length = as_word(bs)
    if printing: print_word_split(length,[0],names={0:'len'})

    n_read = 0
    while n_read<length:
        # Read next 4 byte word
        bs = file.read(4)
        if not bs: return
        if printing: print_word_split(as_word(bs),[0])
        n_read += 4;

SHDR_WORD = as_word([ord(x) for x in 'SHDR'])
SHEX_WORD = as_word([ord(x) for x in 'SHEX'])
RDEF_WORD = as_word([ord(x) for x in 'RDEF'])
OTHER_CHUNKS = ["ICFE", "ISG1", "ISGN", "OSG1", "OSG5", "OSGN", "PCSG", "RDEF", "SDGB", "SFI0", "SHDR", "SHEX", "SPDB", "STAT"]
OTHER_CHUNK_WORDS = [as_word([ord(x) for x in s]) for s in OTHER_CHUNKS]

with open(fname,"rb") as file:

    print()
    while 1:
        # Read next 4 bytes
        bs = file.read(4)
        if not bs: break
        word = as_word(bs)

        if word == SHDR_WORD or word==SHEX_WORD:
            print()
            print(f"\033[0;33m[[[ %s ]]]\033[0m"%('SHDR' if word == SHDR_WORD else 'SHEX'))
            print_word_split(as_word(bs),[0, 8, 16, 24])
            read_SHEX(file)
            print()

        elif word == RDEF_WORD:
            print()
            print(f"\033[0;33m[[[ RDEF ]]]\033[0m")
            print_word_split(as_word(bs),[0, 8, 16, 24])
            read_RDEF(file)
            print()

        elif word in OTHER_CHUNK_WORDS:
            print()
            print(f"\033[0;33m[[[ %s ]]]\033[0m"%(str(bs).split("'")[1]))
            print_word_split(as_word(bs),[0, 8, 16, 24])
            read_generic_chunk(file, True)
            print()

        else:
            print_word_split(as_word(bs),[0],names={0:'?'})


