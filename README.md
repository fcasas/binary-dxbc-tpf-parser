# Binary dxbc-tpf parser

Quick and dirty python script to print all the bits in a dxbc-tpf object file in an organized way.

**Warning:** Currently, the script works in a rather heuristic way, and some instructions are not covered. 

## Usage:

```
python3 binary-dxbc-tpf-parser.py <file>
```

![Screenshot.](./screenshot.png)


